import express from 'express';
import http from 'http';
import { ApolloServer } from '@apollo/server';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import cors from 'cors';
import bodyParser from 'body-parser';
import {expressMiddleware} from '@apollo/server/express4';
import {resolvers, typeDefs} from './graphql';
import mongoose from 'mongoose';
import 'dotenv/config';

async function bootstrap() {
    const app = express();

    const httpServer = http.createServer(app);

    const server = new ApolloServer({
        typeDefs,
        resolvers,
        plugins: [ApolloServerPluginDrainHttpServer({httpServer})]
    });

    await server.start();
    await mongoose.connect(process.env.MONGODB_URL);

    app.use(
        '/',
        cors(),
        bodyParser.json(),
        expressMiddleware(server, {
            context: ({req}) => ({token: req.headers.token})
        })
    );
    const port = process.env.PORT ?? 3000;
    await new Promise((resolve) => httpServer.listen({port}, resolve));

    console.log(`Server ready ad http://localhost:${port}/`);
}

bootstrap().catch(err => console.error(err))